public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1, "John", "Doe", 1000);
        Employee employee2 = new Employee(2, "Mary", "Sidey", 2000);

        System.out.println(employee1.toString());
        System.out.println(employee2.toString());

        //tên đầy đủ
        System.out.println(employee1.getName());
        System.out.println(employee2.getName());

        //lương 1 năm
        System.out.println("salary per year of employee 1 : " + employee1.getAnualSalary());
        System.out.println("salary per year of employee 2 : " + employee2.getAnualSalary());
        
        //tăng lương 20%
        System.out.println("new salary of employee 1 " + employee1.raiseSalary(10));
        System.out.println("new salary of employee 2 " + employee2.raiseSalary(20));
    }
}
