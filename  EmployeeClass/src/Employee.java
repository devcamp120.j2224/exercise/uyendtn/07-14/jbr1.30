public class Employee {
    int id;
    String firstname;
    String lastname;
    int salary;
    
    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Employee() {
    }

    public Employee(int id, String firstname, String lastname, int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getName() {
        return firstname + " " + lastname;
    }

    public int getAnualSalary() {
        return salary*12;
    }

    public int raiseSalary(double percent) {
        return salary *= (1 + percent/100);
    }

    public String toString() {
        return "Employee [id= " + id + ", name = " + this.getName() + ", salary= " + salary + "]";
    }

}
